import React, { CSSProperties, useEffect, useRef, useState } from "react";

type Props = {
  catalogs: AladinCatalog[],
  onViewChanged: (ra: number, dec: number, fov: number) => void
};

const style: CSSProperties = { width: 800, height: 600, background: "#AAA" };

// TODO: Add event listeners for dragging -> updating the view
const SkyView = (props: Props) => {

  const { catalogs, onViewChanged } = props;

  const ref = useRef<HTMLDivElement>(null);
  const [instance, setInstance] = useState<AladinInstance | null>(null);




  useEffect(() => {
    if (ref.current) {
      const el = ref.current;
      setInstance(A.aladin(`#${el.id}`));
      console.error("sky view mounted");

      return () => {
        console.error("sky view unmounted");
        // TODO: remove listeners?
        // Check if everything is correctly cleared.

        // Destroy aladin SkyView
        el.innerHTML = "";
        setInstance(null);
      }
    }

  }, []);

  /**
   * Re add the catalog whenever the instance is changed
   */
  useEffect(() => {
    if (instance) {

      console.log(catalogs);

      for (let catalog of catalogs) {
        instance.addCatalog(catalog);
      }

      instance.on("positionChanged", (_) => {
        const [ra, dec] = instance?.getRaDec();
        const fov = Math.max(...instance?.getFov());
        onViewChanged(ra, dec, fov);
      });

      // TODO: add opZoomChanged

      console.log("catalogs added");
      return () => {
        // This does not clear the layers in the `Manage Layers` toolbox
        instance.removeLayers();
        console.log("catalogs removed");
      }
    }

  }, [catalogs, instance, onViewChanged]);

  function handleDragEnd() {
    console.log(instance?.getRaDec(), instance?.getFov());
  }


  return (
    <div
      onMouseUp={handleDragEnd}
      onMouseLeave={handleDragEnd}
      onWheel={handleDragEnd}
       ref={ref} style={style} id="SkyView"/>
  );
}

export default SkyView;
