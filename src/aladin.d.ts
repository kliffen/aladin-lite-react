// Aladin SkyView Lite is declared as `A` in the global scope

type AladinColorMaps = "cubehelix" | "eosb" | "rainbow" | "grayscale" | "native";

interface AladinImageLayer {
  // TODO: What are it's properties?
}

// TODO: Fix callback function declaration
interface AladinCallback {
  success: (raDec: string) => void;
  error: (err?: any) => void;
}

interface AladinHipsImageFormat {
  imgFormat: "jpg" | "png";
}

interface AladinOptions {
  target?: string;
  cooFrame?: string;
  survey?: string;
  fov?: number;
  showReticle?: boolean;
  showZoomControl?: boolean;
  showFullscreenControl?: boolean;
  showLayersControl?: boolean;
  showGotoControl?: boolean;
  showShareControl?: boolean;
  showSimbadPointerControl?: boolean;
  showFrame?: boolean;
  fullScreen?: boolean;
  reticleColor?: string;
  reicleSize?: number;
};


interface AladinSource {

}

interface AladinCatalog {
  readonly addSources: (sources: AladinSource | AladinSource[]) => void;
  readonly remove: (source: AladinSource) => void;
}

interface AladinInstance {
  readonly getRaDec: () => [number, number];
  readonly getSize: () => [number, number];
  readonly getFov: () => [number, number];
  readonly getFovCorners: (nbSteps?: number) => [number, number][];
  readonly setFov: (degrees: number) => void;
  readonly adjustFovForObject: (target: string) => void;
  readonly setFovRange: (minFov: number, maxFov: number) => void;
  readonly gotoRaDec: (ra: number, dec: number) => void;
  readonly gotoObject: (target: string, callback?: AladinCallback) => void;
  readonly setImageSurvey: (hhipsID: string, hipsName: string, hipsBaseUrl: string, hipsFrame: "equatorial" | "galactic", hipsMaxOrder: number, format: AladinHipsImageFormat) => void;
  readonly getBaseImageLayer: () => AladinImageLayer;
  readonly getColorMap: () => AladinColorMaps;
  readonly displayFITS: (fitsUrl: string) => void;
  readonly addCatalog: (catalog: AladinCatalog) => void;
  readonly removeLayers: () => void;
  // TODO: check if listener can be "cancelled"
  readonly on: (type: "objectHovered" | "objectClicked" | "positionChanged", handler: (obj: any) => void) => void;
};

interface AladinCatalogOptions {
  name?: string;
  shape?: "circle" | "plus" | "rhomb" | "cross" | "triangle" | "square" | HTMLCanvasElement; // JPEG/PNG also supported, but how?
  color?: string;
  sourceSize?: number;
  raField?: string;
  decField?: string;
  labelColumn?: string;
  labelColor?: string;
  labelFont?: string;
  onClick?: "showTable" | "showPopup";
  limit?: number;
}

type AladinCatalogCallback = (sources: AladinSource[]) => void;

interface Aladin {
  readonly aladin: (id: string, options?: AladinOptions) => AladinInstance;
  readonly catalog: (options?: AladinCatalogOptions) => AladinCatalog;
  readonly source: (ra: number, dec: number, data: any) => AladinSource;
  // TODO: add option info
  readonly catalogFromUrl: (url: string, options?: AladinCatalogOptions, successCallback: AladinCatalogCallback, useProxy?: boolean) => AladinCatalog;
  // TODO Add other catalog methods
  // catalogFromSimbad(<target>, <radius-in-degrees>, <catalog-options>?, <successCallback>?)
  // A.catalogFromVizieR(<vizier-cat-id>, <target>, <radius-in-deg>, <cat-options>?, <successCallback>?)
  // A.catalogFromNED(<target>, <radius-in-degrees>, <catalog-options>?, <successCallback>?)
  // Additional optional query options can be specified as a keyword/value dictionary, eg: {"-loc": 500, "-filter": 0}
  // A.catalogFromSkyBot(<ra>, <dec>, <radius>, <epoch>, <query-options>?, <cat-options>?, <successCallback>?)
  // Markers?
  // A.marker(ra, dec, {popupTitle: <title of the popup>, popupDesc: <text (possibly HTML) for the popup>})

  // TODO: Add the final functions
};

declare const A: Aladin;
