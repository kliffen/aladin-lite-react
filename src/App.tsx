import React, { useState } from 'react';
import SkyView from './SkyView';

// TODO: Load sources from somewhere else
const a = A.source(0, 0, { hello: "World", size: 42 });
const catalogA = A.catalog({ name: "A", sourceSize: 20, color: "#fff", onClick: "showPopup" });
catalogA.addSources([a]);

const b = A.source(0, 2, { hello: "universe", size: 42 });
const catalogB = A.catalog({ name: "B", sourceSize: 20, color: "#f00", onClick: "showPopup" });
catalogB.addSources([b]);

function App() {


  const [catalogs, setCatalogs] = useState([catalogA]);

  function handleClickA() {
    console.log("A & B");
    setCatalogs([catalogA, catalogB])
  }
  function handleClickB() {
    console.log("A");
    setCatalogs([catalogA]);
  }

  function handleClickC() {
    catalogA.addSources(A.source(Math.random(), Math.random(), { hello: "World", size: 42 }));
    setCatalogs([catalogA]);
  }

  function handleViewChanged(ra: number, dec: number, fov: number) {
    console.log(ra, dec, fov)
  }

  return (
    <div>
      <SkyView catalogs={catalogs} onViewChanged={handleViewChanged} />
      <button onClick={handleClickA}>Add B</button>
      <button onClick={handleClickB}>Remove B</button>
      <button onClick={handleClickC}>Add new sources</button>
    </div>
  );
}

export default App;
